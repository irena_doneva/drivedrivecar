package com.drivedrive.car.ddc.rest.service;

import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class ConnectionInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
//
//        if (!NetworkUtil.isOnline(MagisApplication.getAppContext())) {
//            Log.d("zz", " no internet connection");
//        }


        Request request = chain.request();
        Response response = chain.proceed(request);


        if (response.code() >= 500) {
            Log.d("zz", "server error response");
            return response;

        }
        if (response.code() >= 400) {
            Log.d("zz", "server error response");
            return response;
        }

        return response;


    }
}
