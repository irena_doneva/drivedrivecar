package com.drivedrive.car.ddc.rest.service;

import com.google.gson.JsonObject;

import org.json.JSONArray;

import java.sql.DataTruncation;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface UrlService {

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
            "Authorization: Bearer "
    })


    /*
    USERS
     */

    @POST("api/v1/users")
    Call<JsonObject> registerUser(@Body JsonObject object);


    @POST("api/v1/users/confirmation")
    Call<JsonObject> confirmRegistration(@Body JsonObject object);

    @POST("api/v1/users/confirmation/send")
    @FormUrlEncoded
    Call<JsonObject> sendConfirmationEmail(@Field("email") String email,
                                           @Field("confirmation_url") String confirmation_url);

    @PUT("api/v1/users/me")
    Call<JsonObject> updateUser(@Header("Authorization") String token,
                                @Body JsonObject object);

    @POST("api/v1/users/me/avatar")
    @FormUrlEncoded
    Call<JsonObject> uploadAvatar(@Header("Authorization") String token);

    @POST("api/v1/users/password/reset")
    @FormUrlEncoded
    Call<JsonObject> resetPassword(@Field("email") String email,
                                   @Field("token") String token,
                                   @Field("password") String password,
                                   @Field("password_confirmation") String passwordConfirmation);


    @POST("api/v1/users/password/send")
    @FormUrlEncoded
    Call<JsonObject> resetLinkEmail(@Field("email") String email,
                                    @Field("reset_url") String resetUrl);

    @POST("api/v1/users/4/devices")
    @FormUrlEncoded
    Call<JsonObject> addDeviceToken(@Header("Authorization") String token,
                                    @Field("token") String deviceToken);

}
