package com.drivedrive.car.ddc.data.security;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.security.KeyPairGeneratorSpec;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.support.annotation.NonNull;

import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Calendar;

import javax.security.auth.x500.X500Principal;

public class KeystoreWraper {

    private static final String PROVIDER_ANDROID_KEY_STORE = "AndroidKeyStore";
    private static final int KEY_SIZE = 1024;
    private KeyStore mAndroidKeyStore;
    private Context context;

    public KeystoreWraper(Context context) {
        this.context = context;
    }

    public KeyPair generateAndroidAsymmetricKeys(@NonNull String alias) {

        String provider = PROVIDER_ANDROID_KEY_STORE;

        AlgorithmParameterSpec spec;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            spec = keyGenParameterSpec(alias);
        } else {
            spec = keyPairGeneratorSpec(alias);
        }

        KeyPairGenerator generator = null;
        KeyPair keyPair = null;
        try {
            generator = KeyPairGenerator.getInstance("RSA", provider);
            generator.initialize(spec);
            keyPair = generator.generateKeyPair();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }

        return keyPair;


    }

    private KeyPairGeneratorSpec keyPairGeneratorSpec(String alias) {
        final Calendar start = Calendar.getInstance();
        final Calendar end = Calendar.getInstance();
        end.add(Calendar.YEAR, 20);
        return new KeyPairGeneratorSpec.Builder(context)
                .setAlias(alias)
                .setEndDate(end.getTime())
                .setStartDate(start.getTime())
                .setSerialNumber(BigInteger.ONE)
                .setSubject(new X500Principal("CN=" + alias))
                .build();

    }

    @TargetApi(Build.VERSION_CODES.M)
    private KeyGenParameterSpec keyGenParameterSpec(String alias) {
        final Calendar start = Calendar.getInstance();
        final Calendar end = Calendar.getInstance();
        end.add(Calendar.YEAR, 20);
        int purpose = KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT;
        return new KeyGenParameterSpec.Builder(alias, purpose)
                .setKeySize(KEY_SIZE)
                .setCertificateSerialNumber(BigInteger.ONE)
                .setCertificateSubject(new X500Principal("CN=" + alias + " CA Certificate"))
                .setCertificateNotBefore(start.getTime())
                .setCertificateNotAfter(end.getTime())
                .setBlockModes(KeyProperties.BLOCK_MODE_ECB)
                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_RSA_PKCS1)
                .build();

    }


    public KeyPair getAsymmetricKeyFromAndroidKeyStore(@NonNull String alias) {
        KeyPair result = null;
        try {
            KeyStore keyStore = createAndroidKeystore();
            PrivateKey privateKey = (PrivateKey) keyStore.getKey(alias, null);
            if (privateKey != null) {
                PublicKey publicKey = keyStore.getCertificate(alias).getPublicKey();
                result = new KeyPair(publicKey, privateKey);
            }
        } catch (KeyStoreException | CertificateException | IOException | NoSuchAlgorithmException | UnrecoverableEntryException e) {

        }
        return result;
    }

    private KeyStore createAndroidKeystore()
            throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException {
        if (mAndroidKeyStore == null) {
            mAndroidKeyStore = KeyStore.getInstance(PROVIDER_ANDROID_KEY_STORE);
        }
        mAndroidKeyStore.load(null);
        return mAndroidKeyStore;
    }

}
