package com.drivedrive.car.ddc.data;


import com.drivedrive.car.ddc.data.model.User;

public interface UserDataSource {

    User getUser(long userId);

    void saveUser(User user);
}
