package com.drivedrive.car.ddc.data.security;


import com.drivedrive.car.ddc.ddcApplication;

import java.security.KeyPair;
import java.util.UUID;

public class EncryptedDatabaseSecretGenerator {

    public static final int DEFAULT_KEY_LENGHT = 256;

    public static KeyPair getEnryptedDbRsaKeys() {
        KeystoreWraper keystoreWraper = new KeystoreWraper(ddcApplication.getAppContext());
        KeyPair keyPair = keystoreWraper.getAsymmetricKeyFromAndroidKeyStore(EncryptedDatabaseKeyProvider.RSA_KEY_ALIAS_DB);
        if(keyPair == null){
            keyPair = keystoreWraper.generateAndroidAsymmetricKeys(EncryptedDatabaseKeyProvider.RSA_KEY_ALIAS_DB);
        }
        return keyPair;
    }

    public static String generateRandomString() {
        String randomString = UUID.randomUUID().toString();
        return randomString.substring(0, 3);

    }
}
