package com.drivedrive.car.ddc.rest.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ErrorBodyResponse {

    @SerializedName("code")
    @Expose
    int code;

    @SerializedName("errors")
    @Expose
    Error errors;

    @SerializedName("message")
    @Expose
    String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Error getErrors() {
        return errors;
    }

    public void setErrors(Error errors) {
        this.errors = errors;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class Error {
        HashMap<String, List<String>> errors;

        public HashMap<String, List<String>> getErrors() {
            return errors;
        }

        public void setErrors(HashMap<String, List<String>> errors) {
            this.errors = errors;
        }
    }





}
