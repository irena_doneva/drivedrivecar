package com.drivedrive.car.ddc.data.migrations;

import org.greenrobot.greendao.database.Database;

public interface Migration {
    Integer getVersion();
    void runMigration(Database db);
}
