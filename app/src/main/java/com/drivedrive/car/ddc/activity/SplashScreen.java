package com.drivedrive.car.ddc.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.drivedrive.car.ddc.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashScreen extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.drive_message)
    TextView driveDriveMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        ButterKnife.bind(this);

        String first = "<font color='#FFFFFF'>drive drive </font>";
        String next = "<font color='#EE0000'>car</font>";
        driveDriveMessage.setText(Html.fromHtml(first + next));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.close_splash:
                Intent intent = new Intent(SplashScreen.this, HomeActivity.class);
                startActivity(intent);
                break;

            case R.id.btn_signin:
                break;

            case R.id.btn_signup:
                break;
        }
    }
}
