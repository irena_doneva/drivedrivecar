package com.drivedrive.car.ddc.data.security;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;


import com.drivedrive.car.ddc.BuildConfig;

import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class EncryptedDatabaseKeyProvider {

    private static final String KEY_PREFS_DB_SECRET = "key_secret";
    public static final String RSA_KEY_ALIAS_DB = "db_key_alias";
    private static final String LOG_TAG = "db_secret";

    public String getSecretPassword(Context context) {
        String decryptedString = null;
        KeyPair keyPair;
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        try {
            keyPair = EncryptedDatabaseSecretGenerator.getEnryptedDbRsaKeys();
        } catch (Exception exception) {

            return null;
        }

        String secretPassword = preferences.getString(KEY_PREFS_DB_SECRET, null);
        if (secretPassword == null) {
            try {
                secretPassword = Crypto.encrypt(keyPair.getPublic(), EncryptedDatabaseSecretGenerator.generateRandomString().getBytes());
                preferences.edit().putString(KEY_PREFS_DB_SECRET, secretPassword).apply();

            } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchProviderException | InvalidKeySpecException e) {
                e.printStackTrace();
            }
        }
        if (BuildConfig.DEBUG) {
            Log.d(LOG_TAG, "DB key: " + secretPassword);
        }
        if (keyPair != null && secretPassword != null) {
            if (keyPair.getPrivate() != null) {
                try {
                    decryptedString = new String(Crypto.decrypt(keyPair.getPrivate(), secretPassword));
                } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchProviderException e) {
                    e.printStackTrace();
                }
            }
        }

        return decryptedString;
    }
}
