package com.drivedrive.car.ddc;

import android.app.Application;


import com.drivedrive.car.ddc.data.migrations.DatabaseUpgradeHelper;
import com.drivedrive.car.ddc.data.model.DaoMaster;
import com.drivedrive.car.ddc.data.model.DaoSession;
import com.drivedrive.car.ddc.data.security.EncryptedDatabaseKeyProvider;
import com.drivedrive.car.ddc.rest.RestClient;

import org.greenrobot.greendao.database.Database;

public class ddcApplication extends Application {

    public static final String BASE_URL = " https://test.drivedrivecar.com/";

    public static final String ENCRYPTED_DB_NAME = "drivecar-db-encrypted";
    public static final boolean ENCRYPTED = true;
    private DaoSession mDaoSession;



    private static RestClient restClient;
    private static ddcApplication app;


    @Override
    public void onCreate() {
        super.onCreate();

        restClient = new RestClient(BASE_URL);
        app = this;
    }


    public static RestClient getRestClient() {
        return restClient;
    }

    public DaoSession getDaoSession() {

        if (mDaoSession != null) {
            return mDaoSession;
        } else {
            return initDB();
        }

    }

    public void setDaoSession(DaoSession daoSession) {
        mDaoSession = daoSession;
    }

    private DaoSession initDB() {
        Database db ;
        DatabaseUpgradeHelper helper = new DatabaseUpgradeHelper(this, ddcApplication.ENCRYPTED ? ddcApplication.ENCRYPTED_DB_NAME : "drive-db");

        String secretPassword = new EncryptedDatabaseKeyProvider().getSecretPassword(this);
        if (secretPassword != null && ddcApplication.ENCRYPTED) {
            db = helper.getEncryptedWritableDb(secretPassword);
        } else {
            db = helper.getWritableDb();
        }

        return new DaoMaster(db).newSession();


    }

    public static Application getAppContext() {
        return getApp();
    }

    public static ddcApplication getApp() {
        return app;

    }

}
