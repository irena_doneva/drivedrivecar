package com.drivedrive.car.ddc.rest;

import com.drivedrive.car.ddc.rest.service.ConnectionInterceptor;
import com.drivedrive.car.ddc.rest.service.UrlService;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {

    private UrlService apiService;
    private Retrofit retrofit;

    OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .addInterceptor(new ConnectionInterceptor())
            .build();

    public RestClient(String baseUrl) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
//                .client(okHttpClient)
                .build();

        this.retrofit = retrofit;
        apiService = retrofit.create(UrlService.class);
        //apiService.reqCountryDetails("");

    }

    public UrlService getUrlService() {
        return apiService;
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }
}
