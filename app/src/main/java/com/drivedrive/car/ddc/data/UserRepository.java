package com.drivedrive.car.ddc.data;

import com.drivedrive.car.ddc.data.local.UserLocalDataSource;
import com.drivedrive.car.ddc.data.model.User;

public class UserRepository implements UserDataSource{

    private static UserRepository mUserRepository;
    private static UserLocalDataSource mUserDataSource;


    private UserRepository(UserLocalDataSource magisUserDataSource) {
        mUserDataSource = magisUserDataSource;

    }

    public static UserRepository getInstance(UserLocalDataSource magisUserDataSource) {
        if (mUserRepository == null) {
            mUserRepository = new UserRepository(magisUserDataSource);
        }

        return mUserRepository;
    }

//    public User loadDeepUser(long id){
//        return mUserDataSource.loadDeepUser(id);
//    }
    @Override
    public User getUser(long userId) {
        return mUserDataSource.getUser(userId);
    }

    @Override
    public void saveUser(User user) {
        mUserDataSource.saveUser(user);
    }
}
