package com.drivedrive.car.ddc.data.local;

import android.content.Context;

import com.drivedrive.car.ddc.data.UserDataSource;
import com.drivedrive.car.ddc.data.model.DaoSession;
import com.drivedrive.car.ddc.data.model.User;
import com.drivedrive.car.ddc.data.model.UserDao;
import com.drivedrive.car.ddc.ddcApplication;

public class UserLocalDataSource implements UserDataSource {

    private static UserLocalDataSource mUserLocalDataSource;

    DaoSession daoSession;
    UserDao magisUserDao;


    private UserLocalDataSource(Context context) {
        daoSession =  ddcApplication.getApp().getDaoSession();
        magisUserDao = daoSession.getUserDao();
    }


    public static UserLocalDataSource getInstance(Context context) {
        if (mUserLocalDataSource == null) {
            mUserLocalDataSource = new UserLocalDataSource(context);
        }
        return mUserLocalDataSource;
    }

//    public User loadDeepUser(long id){
//        return  magisUserDao.loadDeep(id);
//    }

    @Override
    public User getUser(long userId) {
        return  magisUserDao.load(userId);
    }

    @Override
    public void saveUser(User user) {
        if(magisUserDao.load(user.getId()) == null){
            magisUserDao.insert(user);
        }else{
            magisUserDao.update(user);
        }
    }
}
